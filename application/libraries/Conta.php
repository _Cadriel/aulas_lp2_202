<?php
    include_once APPPATH.'libraries/util/CI_Object.php';
    defined ('BASEPATH') OR exit ('No direct script access allowed');    
    
    class conta extends CI_Object{
        
        function cria($data){
            $this->db->insert('conta', $data);
            return $this->db->insert_id();
        }

        public function lista($tipo, $mes = 0, $ano = 0){
            $data = ['tipo' => $tipo, 'mes' => $mes, 'ano' => $ano];
            $res = $this->db->get_where('conta', $data);
            return $res->result_array();
        }
    }
?>